#include <iostream>

#include "libexample.h"


int main() {
    std::cout << "add(1, 2) = " << add(1, 2) << std::endl
              << "sub(1, 2) = " << sub(1, 2) << std::endl;
}
