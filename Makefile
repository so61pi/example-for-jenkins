RM			?= rm
BUILDDIR	:= build

.PHONY: all
all:
	mkdir -p $(BUILDDIR)
	cd $(BUILDDIR) && cmake .. && cmake --build .

.PHONY: test
test:
	cd $(BUILDDIR) && ctest

.PHONY: clean
clean:
	$(RM) -r $(BUILDDIR)
