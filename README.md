**overview**

simple example repo for testing jenkins


**install cmake**

```shell
sudo apt -y install cmake
```


**get & run jenkins**

```shell
wget http://mirrors.jenkins.io/war-stable/latest/jenkins.war
java -jar jenkins.war
# xdg-open 'http://localhost:8080'
```


**plugin**

- Bitbucket Plugin
- Test Results Analyzer Plugin
- disk-usage plugin


**note**

- when create admin user, please fill all fields
