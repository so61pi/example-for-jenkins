#include <gtest/gtest.h>

#include "libexample.h"


TEST(AddTest, PositiveInputs) {
    EXPECT_EQ(3, add(1, 2));
}

TEST(AddTest, NegativeInputs) {
    EXPECT_EQ(-3, add(-1, -2));
}

TEST(SubTest, PositiveInputs) {
    EXPECT_EQ(-1, sub(1, 2));
}

TEST(SubTest, NegativeInputs) {
    EXPECT_EQ(1, sub(-1, -2));
}

TEST(MulTest, PositiveInputs) {
    EXPECT_EQ(2, mul(1, 2));
}
